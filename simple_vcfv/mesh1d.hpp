/* =============================================================================

 Copyright (c) 2017 F. Georget <fabien.georget@epfl.ch> EPFL
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SIMPLEVCFV_MESH_HPP
#define SIMPLEVCFV_MESH_HPP

#include "types.hpp"

namespace simple_vcfv {

class Mesh1D
{
public:
    static std::shared_ptr<Mesh1D> make(const Vector& coordinates, const Vector& cross_sections) {
        return std::make_shared<Mesh1D>(coordinates, cross_sections);
    }

    Mesh1D(const Vector& coordinates, const Vector& cross_sections):
        m_coordinates(coordinates),
        m_cross_sections(cross_sections)
    {}

    Index nen() {
        return 2;
    }

    Index nel() {
        return m_coordinates.rows()-1;
    }

    Index nnp() {
        return m_coordinates.rows();
    }

    double coordinate(Index node) {
        return m_coordinates(node);
    }
    double cross_section(Index element) {
        return m_cross_sections(element);
    }
    double dx(Index element) {
        return (m_coordinates(element+1) - m_coordinates(element));
    }

private:
     Vector m_coordinates;
     Vector m_cross_sections;

};
} // namespace simple_vcfv

#endif // SIMPLEVCFV_MESH_HPP
