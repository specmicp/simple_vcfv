/* =============================================================================

 Copyright (c) 2017 F. Georget <fabien.georget@epfl.ch> EPFL
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SIMPLEVCFV_DIFFUSION_HPP
#define SIMPLEVCFV_DIFFUSION_HPP


#include "types.hpp"
#include "mesh1d.hpp"

namespace simple_vcfv {


class DiffusionProgram
{
public:
    DiffusionProgram(std::shared_ptr<Mesh1D> the_mesh, const Vector& diff_coeffs, const Vector& porosity, const Matrix& bcs);

    static std::shared_ptr<DiffusionProgram> make(std::shared_ptr<Mesh1D> the_mesh, const Vector& diff_coeffs, const Vector& porosity, const Matrix& bcs) {
        return std::make_shared<DiffusionProgram>(the_mesh, diff_coeffs, porosity, bcs);
    }

    Eigen::Index ndof() {
        return 1;
    }

    void assemble_system(const Vector& predictors, double alpha, double timestep, Vector& RHS, Matrix& LHS);

    Index id_equation(Index node, Index dof) {
        return m_ideq(node, dof);
    }

private:

    void compute_element_lhs_rhs(Index element, const Vector& predictors, double alpha, double timestep, Vector& rhs_e, Matrix& lhs_e);

    Eigen::Matrix2Xd element_mass_matrix(Index element);

    Eigen::Matrix2Xd element_stiffness_matrix(Index element);

    void init_number_equations(const Matrix& bcs);

    std::shared_ptr<Mesh1D> m_mesh;
    Vector m_diff_coeffs;
    Vector m_porosity;

    Eigen::Index m_neq;
    Matrix m_ideq;
};



class ParabolicDriver
{
public:

    ParabolicDriver(std::shared_ptr<Mesh1D> the_mesh, std::shared_ptr<DiffusionProgram> the_program):
        m_mesh(the_mesh),
        m_program(the_program)
    {}

    static std::shared_ptr<ParabolicDriver> make(std::shared_ptr<Mesh1D> the_mesh, std::shared_ptr<DiffusionProgram> the_program) {
        return std::make_shared<ParabolicDriver>(the_mesh, the_program);
    }

    bool solve_timestep(Vector& variables, Vector& velocities, double alpha, double timestep);

    void compute_predictors(Vector& predictors, const Vector& variables, const Vector& velocities, double alpha, double timestep);
    void set_system(const Vector& predictors, double alpha, double timestep, Vector& RHS, Matrix& LHS);

    void update_variables(const Vector& update, Vector& variables, const Vector& predictors, Vector& velocities, double alpha, double timestep);

private:
    std::shared_ptr<Mesh1D> m_mesh;
    std::shared_ptr<DiffusionProgram> m_program;

};

} // namespace simple_vcfv


#endif // SIMPLEVCFV_DIFFUSION_HPP
