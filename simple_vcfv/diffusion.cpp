/* =============================================================================

 Copyright (c) 2017 F. Georget <fabien.georget@epfl.ch> EPFL
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "diffusion.hpp"

#include <iostream>

namespace simple_vcfv {

Index number_equations(Index ndf, Index nodes, Matrix& bcs)
{
    Index neq = 0;
    for (Index node=0; node<nodes; ++node) {
        for (Index dof=0; dof<ndf; ++dof) {
            if (bcs(node, dof) != no_equation) {
                bcs(node, dof) = neq;
                ++neq;
            }
        }
    }
    return neq;
}


DiffusionProgram::DiffusionProgram(std::shared_ptr<Mesh1D> the_mesh, const Vector& diff_coeffs, const Vector& porosity, const Matrix& bcs):
    m_mesh(the_mesh),
    m_diff_coeffs(diff_coeffs),
    m_porosity(porosity)
{
    init_number_equations(bcs);
}

void DiffusionProgram::init_number_equations(const Matrix& bcs) {

    m_ideq = Matrix(bcs);
    m_neq = number_equations(ndof(), m_mesh->nnp(), m_ideq);
}

void DiffusionProgram::assemble_system(const Vector& predictors, double alpha, double timestep, Vector& RHS, Matrix& LHS) {
    RHS.setZero(m_neq);
    LHS.setZero(m_neq, m_neq);

    for (Eigen::Index element=0; element<m_mesh->nel(); ++element) {
        Matrix lhs_e;
        Vector rhs_e;
        compute_element_lhs_rhs(element, predictors, alpha, timestep, rhs_e, lhs_e);

        for (Eigen::Index enode=0; enode < m_mesh->nen(); ++enode) {
            Eigen::Index node = element+enode;
            auto id_eq = m_ideq(node, 0);
            if (id_eq == no_equation) continue;
            RHS(id_eq) += rhs_e(enode);

            for (Eigen::Index cenode=0; cenode < m_mesh->nen(); ++cenode) {
                Eigen::Index cnode = element+cenode;
                auto id_eqc = m_ideq(cnode, 0);
                if (id_eqc == no_equation) continue;
                LHS(id_eq, id_eqc) += lhs_e(enode, cenode);
            }
        }
    }
}

void DiffusionProgram::compute_element_lhs_rhs(Index element, const Vector& predictors, double alpha, double timestep, Vector& rhs_e, Matrix& lhs_e) {

    auto Me = element_mass_matrix(element);
    auto Ke = element_stiffness_matrix(element);

    Eigen::Vector2d d_e;
    d_e << predictors(element), predictors(element+1);

    lhs_e = Me+alpha*timestep*Ke;
    //rhs = external_flux - Me*ve - Ke*d_e
    rhs_e = - Ke * d_e;
}

Eigen::Matrix2Xd DiffusionProgram::element_mass_matrix(Index element) {
    return m_porosity(element)*m_mesh->dx(element)*m_mesh->cross_section(element)/2.0*Eigen::Matrix2Xd::Identity(2, 2);
}

Eigen::Matrix2Xd DiffusionProgram::element_stiffness_matrix(Index element) {
    Eigen::Matrix2d matrix_;
    matrix_ << 1, -1, -1, 1;
    return m_diff_coeffs(element)*m_mesh->cross_section(element)/m_mesh->dx(element)*matrix_;
}

bool ParabolicDriver::solve_timestep(Vector& variables, Vector& velocities, double alpha, double timestep) {
    Vector RHS;
    Matrix LHS;

    Vector predictors;

    compute_predictors(predictors, variables, velocities, alpha, timestep);
    set_system(predictors, alpha, timestep, RHS, LHS);
    Vector du = LHS.colPivHouseholderQr().solve(RHS);
    update_variables(du, variables, predictors, velocities, alpha, timestep);

    return true;
}

void ParabolicDriver::compute_predictors(Vector& predictors, const Vector& variables, const Vector& velocities, double alpha, double timestep) {
    predictors =  variables + (1.0 - alpha)*timestep*velocities;
}

void ParabolicDriver::set_system(const Vector& predictors, double alpha, double timestep, Vector& RHS, Matrix& LHS) {
    m_program->assemble_system(predictors, alpha, timestep, RHS, LHS);
}

void ParabolicDriver::update_variables(const Vector& update, Vector& variables, const Vector& predictors, Vector& velocities, double alpha, double timestep) {
    for (auto node = 0; node < m_mesh->nnp(); ++node) {
        for (auto dof=0; dof < m_program->ndof(); ++dof) {
            auto id_eq = m_program->id_equation(node, dof);
            if (id_eq == no_equation) continue;
            velocities(node*m_program->ndof()+dof) = update(id_eq);
        }
    }
    variables = predictors + alpha*timestep*velocities;
}

} // namespace simple_vcfv

