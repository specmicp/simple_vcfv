# Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton University
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#    * Neither the name of SpecMiCP nor the
#        names of its contributors may be used to endorse or promote products
#        derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



# Includes from Eigen
#
# This is kept simple on purpose
cdef extern from "<Eigen/Dense>" namespace "Eigen":
    ctypedef int Index
    
    cdef cppclass Matrix[T, U, V]:
        Matrix()
        T& operator()(int, int)
        T& operator()(int)

    # A matrix
    cdef cppclass MatrixXd:
        MatrixXd()
        MatrixXd(int, int)
        MatrixXd(const MatrixXd& other)
        void setZero()
        void setZero(int, int)
        float& operator()(int, int) # read access

    # a vector
    cdef cppclass VectorXd:
        VectorXd()
        VectorXd(int)
        VectorXd(const VectorXd& other)
        void setZero()
        void setZero(int)

        float& operator()(int) # read access

# write
cdef extern from "eigen_set.hpp":
    cdef void vector_setitem(VectorXd&, int, float)
    cdef void vector_additem(VectorXd&, int, float)
    cdef void matrix_setitem(MatrixXd&, int, int, float)
    cdef void matrix_additem(MatrixXd&, int, int, float)
