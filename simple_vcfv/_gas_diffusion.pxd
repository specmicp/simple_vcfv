# Copyright (c) 2017 Fabien Georget <fabien.georget@epfl.ch>, EPFL
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#    * Neither the name of SpecMiCP nor the
#        names of its contributors may be used to endorse or promote products
#        derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from eigen cimport VectorXd, MatrixXd, Index
from memory cimport shared_ptr
from libcpp cimport bool

cdef extern from "types.hpp" namespace "simple_vcfv":
    ctypedef VectorXd Vector
    ctypedef MatrixXd Matrix

cdef extern from "mesh1d.hpp" namespace "simple_vcfv":
    cdef cppclass CppMesh1D "simple_vcfv::Mesh1D":
       CppMesh1D(const Vector&, const Vector&)
       
       @staticmethod
       shared_ptr[CppMesh1D] make(const Vector&, const Vector&)
       
       Index nen()
       Index nel()
       Index nnp()
       
       double coordinate(Index node)
       double cross_section(Index node)
       double dx(Index element)
       
       
cdef extern from "diffusion.hpp" namespace "simple_vcfv":
    cdef cppclass CppDiffusionProgram "simple_vcfv::DiffusionProgram":
        CppDiffusionProgram(shared_ptr[CppMesh1D], const Vector&, const Vector&, const Matrix&)
        
        @staticmethod
        shared_ptr[CppDiffusionProgram] make(shared_ptr[CppMesh1D], const Vector&, const Vector&, const Matrix&)
        
        
    cdef cppclass CppParabolicDriver "simple_vcfv::ParabolicDriver":
        CppParabolicDriver(shared_ptr[CppMesh1D], shared_ptr[CppDiffusionProgram])
        
        bool solve_timestep(Vector&, Vector&, double alpha, double timestep)
        
        @staticmethod
        shared_ptr[CppParabolicDriver] make(shared_ptr[CppMesh1D], shared_ptr[CppDiffusionProgram])
        
        
cdef class Configurator:
    cdef shared_ptr[CppMesh1D] c_mesh
    cdef shared_ptr[CppDiffusionProgram] c_program
    cdef shared_ptr[CppParabolicDriver] c_driver
    cdef VectorXd c_vars
    cdef VectorXd c_velocity
    cdef dict conf
    
    cdef void _configure(self, dict data, double diffusion_sample)
    
    cdef shared_ptr[CppMesh1D] get_mesh(self)
    cdef shared_ptr[CppDiffusionProgram] get_program(self)
    cdef shared_ptr[CppParabolicDriver] get_driver(self)
    cdef VectorXd get_vars_0(self)
    cdef VectorXd get_velocity_0(self)
       
    
cdef class Mesh1D:
    cdef shared_ptr[CppMesh1D] c_mesh
    cdef shared_ptr[CppMesh1D] get_cpp_ptr(self)
    
cdef class DiffusionProgram:
    cdef shared_ptr[CppDiffusionProgram] c_program
    cdef shared_ptr[CppDiffusionProgram] get_cpp_ptr(self)
    
    
    
cdef class ParabolicDriver:
    cdef VectorXd c_vars
    cdef VectorXd c_velocity
    cdef double alpha
    
    cdef shared_ptr[CppParabolicDriver] c_driver
    cdef shared_ptr[CppParabolicDriver] get_cpp_ptr(self) 
    
    cdef void set_variable(self, const Vector&)