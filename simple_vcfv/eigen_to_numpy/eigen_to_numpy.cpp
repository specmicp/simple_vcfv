/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */


#include "eigen_to_numpy.hpp"

#include <stdexcept>

PyObject* eigen_vector_to_numpy_array(const Eigen::VectorXd& vector)
{
    Py_Initialize();
    import_array();
    npy_intp dims[1] = {static_cast<npy_intp>(vector.rows())};
    PyObject* array_py_obj = PyArray_SimpleNew(1, dims, NPY_DOUBLE);
    if (array_py_obj == NULL) {
        throw std::runtime_error("Failed to create numpy array");
    }

    PyArrayObject* array = reinterpret_cast<PyArrayObject*>(array_py_obj);
    memcpy((double*) PyArray_BYTES(array), vector.data(), vector.rows()*sizeof(double));
    //double* arr = (double*) PyArray_DATA(array);
    //for (std::size_t ind=0; ind< vector.rows(); ++ind)
    //{
    //    arr[ind] = vector(ind);
    //}


    return array_py_obj;
}
