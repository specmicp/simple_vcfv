# Copyright (c) 2017 Fabien Georget <fabien.georget@epfl.ch>, EPFL
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#    * Neither the name of SpecMiCP nor the
#        names of its contributors may be used to endorse or promote products
#        derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import numpy as np
import scipy.optimize as sco
import matplotlib.pyplot as plt
import time

import simple_vcfv._gas_diffusion as sgd

def get_default_data():
    """Return the default configuration for the experiment
    
    Keys:
        nx_sample:  Number of nodes in sample
        length_sample: Length of sample (cm)
        diameter_sample: Diameter of sample (cm)
        porosity_sample: Porosity of sample
        init_O2_sample: Initial O2 concentration in sample (if None, same as chamber)
    
        nx_chamber: Number of nodes in chamber
        length_chamber: Length of chamber (cm)
        diameter_chamber: Diameter of chamber (cm, equivalent)
        nx_indent: Number of nodes in sensor indent 
        length_indent: Length of sensor indent (cm)
        diameter_indent: Diameter sensor indent (cm)
        diffusion_air: Diffusion coefficient of oxygen in nitrogen (cm^2/s)
    
        init_O2_chamber: Default O2 concentration in chamber (if None, set automatically)
    
        alpha: time integration selector, 0 explicit, 0.5 crank-nicholson, 1 implicit
        dt_factor: multiplication factor for the timestep
    
        O2_air: O2 concentration in ambient air
        calibration_point: O2 concentration mesured by sensor in ambiant (if None, last point)
    
    """
    default_data = {}
    default_data["nx_sample"] = 2         # Number of nodes in sample
    default_data["length_sample"] = 0.10     # Length of sample (cm)
    default_data["diameter_sample"] = 1.50   # Diameter of sample (cm)
    default_data["porosity_sample"] = 0.2    # Porosity of sample
    default_data["init_O2_sample"] = None    # Initial O2 concentration in sample (if None, same as chamber)
    # ----
    default_data["nx_chamber"] = 5           # Number of nodes in chamber
    default_data["length_chamber"] = 3.40    # Length of chamber (cm)
    default_data["diameter_chamber"] = 2.764 # Diameter of chamber (cm, equivalent)
    default_data["nx_indent"]  = 3           # Number of nodes in sensor indent 
    default_data["length_indent"] = 0.7      # Length of sensor indent (cm)
    default_data["diameter_indent"] = 0.7    # Diameter sensor indent (cm)
    default_data["diffusion_air"] = 0.202    # Diffusion coefficient of oxygen in nitrogen (cm^2/s)
    default_data["init_O2_chamber"] = None      # Default O2 concentration in chamber (if None, set automatically)
    # ---
    default_data["alpha"] = 1.0               # 0 explicit, 0.5 crank-nicholson, 1 implicit
    default_data["dt_factor"] = 10.0          # multiplication factor for the timestep
    # ---
    default_data["O2_air"] = 21                # O2 concentration in ambient air
    default_data["calibration_point"] = None   # O2 concentration mesured by sensor in ambiant (if None, last point)
    
    return default_data

def get_func_diffusion(data):
    """Return the function to fit"""
    def func_diffusion(xts, diffusion_sample):
        
        conf = sgd.Configurator(data, diffusion_sample)
        driver = sgd.ParabolicDriver(conf)
     
        conc = []
        tot_time=0
        i=0
        while tot_time<xts[-1]:
            dt = conf["dt_factor"]*conf["explicit_dt"]
            driver.solve_timestep(dt)
            tot_time += dt
            if tot_time > xts[i]:
                conc.append(float(driver.get_solution()[-1]))
                i+=1
        return np.array(conc)
    return func_diffusion

def extract_from_csv(filename, data):
    """Obtain the experimental data"""
    exp_data = np.loadtxt(filename, delimiter=",", skiprows=1)
    exp_data[:,0] -= exp_data[0, 0] # start at t = 0
    exp_data[:,0] *= 3600       # transform time unit : h -> s
    
    if data["calibration_point"] is None:
        calib = exp_data[-1, 1]
    else:
        calib = data["calibration_point"]
    
    exp_data[:,1] *= data["O2_air"]/calib # normalize data
    
    return exp_data

def fit_experiment(filename, data, initial_guess):
    """Fit the experiments to obtain the diffusion coefficient.
    
    Filename is a filepath to a csv file containing data from an experiment.
    
    Data is the configuration data.
    
    Initial guess is a guess for the diffusion coefficient value in m^2/s"""
    work_data = dict(data)
    
    exp_data = extract_from_csv(filename, work_data)
    ts = exp_data[:, 0]
    ys = exp_data[:, 1]
    
    if work_data["init_O2_chamber"] is None:
        work_data["init_O2_chamber"] = ys[0]
    if work_data["init_O2_sample"] is None:
        work_data["init_O2_sample"] = work_data["init_O2_chamber"]
    
    function_to_fit = get_func_diffusion(work_data)
    popt, pcov = sco.curve_fit(function_to_fit, ts, ys, p0=[initial_guess*1e4])
    De = popt[0]*1e-4
    perr = np.sqrt(pcov[0])[0]
    print("Analysis for {0}: \n - De = {1} m^2/s\n - perr = {2}".format(filename, popt[0]*1e-4, perr))
    
    fitted_ys = function_to_fit(ts, popt[0])
    
    return ((De, perr), (ts, ys, fitted_ys))

def quick_plot(sol, label):
    """Plot a comparison between experiment and the fitted data"""
    ts, ys, fitted_ys = sol
    
    ts_h = ts/3600
    plt.plot(ts_h, ys, "+", label=label)
    plt.plot(ts_h, fitted_ys, label="fit")
    
    plt.xlabel("time (h)")
    plt.ylabel("% O2 in N2")
    plt.legend()
    plt.show()
    
def save_results(filename, fit_out, sol):
    "Save results in filename"""
    (De, perr) = fit_out
    (ts, ys, fitted_ys) = sol
    
    header="Gas diffusion fit - {0}\nDe: {1} m^2/s  perr={2}\ntime(s) \t calibrated experiments (%O2 in N2) \t fitted concentration (%O2 in N2)".format(
        time.asctime(time.localtime()), De, perr
    )
    np.savetxt(filename, np.stack((ts, ys, fitted_ys), axis=1), delimiter="\t", header=header, fmt="%.4e")