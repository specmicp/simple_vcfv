# Copyright (c) 2017 Fabien Georget <fabien.georget@epfl.ch>, EPFL
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#    * Neither the name of SpecMiCP nor the
#        names of its contributors may be used to endorse or promote products
#        derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from eigen cimport vector_setitem, matrix_setitem
from eigen_to_numpy cimport eigen_vector_to_numpy_array
import numpy as np

def cross_section(diameter):
    return np.pi*(diameter/2)**2

cdef class Configurator:
    def __init__(self, dict data_sample, double diffusion_sample):
        self._configure(data_sample, diffusion_sample)

    cdef void _configure(self, dict data, double diffusion_sample):
        
        self.conf = data
        
        nnp = data["nx_sample"] + data["nx_chamber"] + data["nx_indent"] - 1
        nel = nnp - 1
        
        cdef VectorXd vars_0, velocity_0
        cdef VectorXd porosity
        cdef VectorXd diff_coeff
        cdef VectorXd coordinates
        cdef VectorXd cross_sections
    
        cdef MatrixXd bcs
    
        vars_0.setZero(nnp)
        velocity_0.setZero(nnp)
        porosity.setZero(nel)
        diff_coeff.setZero(nel)
        coordinates.setZero(nnp)
        cross_sections.setZero(nel)
        bcs.setZero(nnp, 1)
    
        vector_setitem(vars_0, 0, 21)
        vector_setitem(coordinates, 0, 0.0)
        matrix_setitem(bcs, 0, 0, -1)

        node = 0 #1
        element = -1
        
        if data["init_O2_chamber"] is None or data["init_O2_sample"] is None:
            raise RuntimeError("The keys 'init_O2_chamber' and 'init_O2_sample' must be set")
    
        dx_sample = data["length_sample"] / data["nx_sample"]
        for i in range(data["nx_sample"]):
            node = node + 1
            element = element + 1
            vector_setitem(coordinates, node, coordinates(node-1) + dx_sample)
            vector_setitem(cross_sections, element, cross_section(data["diameter_sample"]))
            vector_setitem(porosity, element, data["porosity_sample"])
            vector_setitem(diff_coeff, element, diffusion_sample)
            vector_setitem(vars_0, node, data["init_O2_sample"])

        dx_chamber = data["length_chamber"] / data["nx_chamber"]
        for i in range(1, data["nx_chamber"]):
            node = node + 1
            element = element + 1
            vector_setitem(coordinates, node, coordinates(node-1) + dx_chamber)
            vector_setitem(cross_sections, element, cross_section(data["diameter_chamber"]))
            vector_setitem(porosity, element, 1.0)
            vector_setitem(diff_coeff, element, data["diffusion_air"])
            vector_setitem(vars_0, node, data["init_O2_chamber"])
            
        dx_captor = data["length_indent"]/data["nx_indent"]
        for i in range(1, data["nx_indent"]):
            node = node + 1
            element = element + 1
            vector_setitem(coordinates, node, coordinates(node-1) + dx_captor)
            vector_setitem(cross_sections, element, cross_section(data["diameter_indent"]))
            vector_setitem(porosity, element, 1.0)
            vector_setitem(diff_coeff, element, data["diffusion_air"])
            vector_setitem(vars_0, node, data["init_O2_chamber"])
            
        explicit_dt = np.min([dx_sample**2/diffusion_sample, dx_chamber**2/data["diffusion_air"], dx_captor**2/data["diffusion_air"]])
        self.conf["explicit_dt"] = explicit_dt
        #return Mesh1D(coordinates, cross_sections), porosity, diff_coeff, explicit_dt, vars_0
            
        self.c_mesh = CppMesh1D.make(coordinates, cross_sections)
        self.c_program = CppDiffusionProgram.make(self.c_mesh, diff_coeff, porosity, bcs)
        self.c_driver = CppParabolicDriver.make(self.c_mesh, self.c_program)
        
        self.c_vars = vars_0
        self.c_velocity = velocity_0
        
    
    cdef shared_ptr[CppMesh1D] get_mesh(self):
        return self.c_mesh
    cdef shared_ptr[CppDiffusionProgram] get_program(self):
        return self.c_program
    cdef shared_ptr[CppParabolicDriver] get_driver(self):
        return self.c_driver
    cdef VectorXd get_vars_0(self):
        return self.c_vars
    cdef VectorXd get_velocity_0(self):
        return self.c_velocity
        
    def __getitem__(self, key):
        return self.conf[key]
    
    
    
cdef class Mesh1D:
    def __cinit__(self, Configurator configurator):
        self.c_mesh = configurator.get_mesh()
    
    cdef shared_ptr[CppMesh1D] get_cpp_ptr(self):
        return self.c_mesh
    
cdef class DiffusionProgram:
    def __cinit__(self, Configurator configurator):
        self.c_program = configurator.get_program()
    
    cdef shared_ptr[CppDiffusionProgram] get_cpp_ptr(self):
        return self.c_program
    
cdef class ParabolicDriver:
    def __cinit__(self, Configurator configurator):
        self.c_driver = configurator.get_driver()
        self.c_vars = configurator.get_vars_0()
        self.c_velocity = configurator.get_velocity_0()
    
        self.alpha = configurator.conf["alpha"]
    
    cdef shared_ptr[CppParabolicDriver] get_cpp_ptr(self):
        return self.c_driver
    
    cdef void set_variable(self, const VectorXd& variables):
        self.c_vars = variables
    
    def solve_timestep(self, double timestep):
        self.c_driver.get().solve_timestep(self.c_vars, self.c_velocity, self.alpha, timestep)
        
    def get_solution(self):
        return <object> eigen_vector_to_numpy_array(self.c_vars)
        