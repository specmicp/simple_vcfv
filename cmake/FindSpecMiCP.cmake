# Find SpecMiCP
# --------------------
# The following variables are defined :
#
#   - SPECMICP_FOUND : true if specmicp is found
#   - SPECMICP_INCLUDE_DIR : the include dir
#   - SPECMICP_LIBRARIES : The SpecMiCP libraries
#   - REACTMICP_LIBRARIES : The ReactMiCP libraries
#   - SPECMICP_ALL_LIBRARIES : All the libraries defined by SpecMiCP
#   - SPECMICP_COMMON_LIBRARY : The specmicp_common library
#   - SPECMICP_DATABASE_LIBRARY : the specmicp_database library
#   - SPECMICP_LIBRARY : the specmicp library
#   - DFPM_LIBRARY : the database library
#   - REACTMICP_LIBRARY : the reactmicp library
#
#   To help the search the variables SPECMICP_LIBS_DIR (directory of the library)
#   and SPECMICP_INCLUDE_DIR can be set.
#
# This other libraries are required :
#
#   - Boost
#   - Eigen3
#
# Also a c++11 compiler is required
#
#
# copyright (c) 2015 Fabien georget <fabieng@princeton.edu>
# Redistribution and use is allowed according to the terms of the 3-clause BSD license.

# Find the includes requirements
# ------------------------------
if (${SpecMiCP_FIND_REQUIRED})
    find_package(Eigen3 REQUIRED)
    include(FindPkgConfig)
    pkg_check_modules(YAML REQUIRED yaml-cpp>=0.5)
else()
    find_package(Eigen3)
endif()

# Find the libraries
# ------------------
if (NOT SPECMICP_ALL_LIBRARIES)
# specmicp_common
if (NOT SPECMICP_COMMON_LIBRARY)
find_library(SPECMICP_COMMON_LIBRARY
    NAMES specmicp_common libspecmicp_common
    HINTS ${SPECMICP_LIBS_DIR}
)
endif()
# specmicp_database
if (NOT SPECMICP_DATABASE_LIBRARY)
find_library(SPECMICP_DATABASE_LIBRARY
    NAMES specmicp_database libspecmicp_database
    HINTS ${SPECMICP_LIBS_DIR}
)
endif()
# specmicp
if (NOT SPECMICP_LIBRARY)
find_library(SPECMICP_LIBRARY
    NAMES specmicp libspecmicp
    HINTS ${SPECMICP_LIBS_DIR}
)
endif()
# dfpm
if (NOT DFPM_LIBRARY)
find_library(DFPM_LIBRARY
    NAMES dfpm libdfpm
    HINTS ${SPECMICP_LIBS_DIR}
)
endif()
# reactmicp
if (NOT REACTMICP_LIBRARY)
find_library(REACTMICP_LIBRARY
    NAMES reactmicp libreactmicp
    HINTS ${SPECMICP_LIBS_DIR}
)
endif()

set(SPECMICP_LIBRARIES
    ${SPECMICP_LIBRARY} ${SPECMICP_DATABASE_LIBRARY} ${SPECMICP_COMMON_LIBRARY} ${YAML_LIBRARIES}
)
set(REACTMICP_LIBRARIES
    ${REACTMICP_LIBRARY} ${DFPM_LIBRARY} ${SPECMICP_LIBRARIES}
)

set(SPECMICP_ALL_LIBRARIES
    ${REACTMICP_LIBRARIES}
)
endif()

# Find the includes
# -----------------
if (NOT SPECMICP_INCLUDE_DIR)
    find_path(SPECMICP_INCLUDE_DIR
    NAMES specmicp/specmicp.hpp
    PATHS ${CMAKE_INSTALL_PREFIX}/include
    )
endif()

# Find the database directory
# ---------------------------
if (NOT SPECMICP_DATABASE_DIR)
    find_path(SPECMICP_DATABASE_DIR
    NAMES cemdata.js
    HINTS ${SPECMICP_INCLUDE_DIR}/../share/specmicp/database
    PATHS ${CMAKE_INSTALL_PREFIX}/share/specmicp/database
    )
endif()

# The complete set of required includes
# -------------------------------------
if (SPECMICP_INCLUDE_DIR)
    set(SPECMICP_INCLUDE_DIR ${SPECMICP_INCLUDE_DIR} ${EIGEN3_INCLUDE_DIR} ${YAML_INCLUDE_DIRS})
endif()


# Check that everything is ok
# ---------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SpecMiCP DEFAULT_MSG SPECMICP_ALL_LIBRARIES SPECMICP_INCLUDE_DIR SPECMICP_DATABASE_DIR)

mark_as_advanced(
    SPECMICP_INCLUDE_DIR
    SPECMICP_ALL_LIBRARIES
    REACTMICP_LIBRARIES SPECMICP_LIBRARIES
    SPECMICP_LIBRARY SPECMICP_DATABASE_LIBRARY DFPM_LIBRARY REACTMICP_LIBRARY
    SPECMICP_DATABASE_DIR
)

# compilation flag required :
# -----------------------------

include(CheckCXXCompilerFlag)

if(NOT CX11_FLAG)
    check_cxx_compiler_flag("-std=c++11" HAS_CXX11)
    if(NOT HAS_CXX11)
        message(FATAL_ERROR "A c++11 compatible compiler is necessary")
    else()
        set(CXX11_FLAG "-std=c++11")
    endif()
endif()
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX11_FLAG}")
