# This file find the numpy include directory
#
# Define: NUMPY_INCLUDE_DIRS : the directory to numpy includes


#assume python interpreter waas found

if (NOT NUMPY_FOUND)
if (NOT NUMPY_INCLUDE_DIRS)

    # First we check that numpy can be imported
    execute_process(
        COMMAND ${PYTHON_EXECUTABLE} "-c" "import numpy"
        RESULT_VARIABLE RETCODE_IMPORT_NUMPY
    )

    if (${RETCODE_IMPORT_NUMPY} GREATER 0)
        message(FATAL_ERROR "Numpy could not be loaded by python interpreter")
    endif()



    # Then we obtain the include directory using
    execute_process(
        COMMAND ${PYTHON_EXECUTABLE} "-c" "import numpy; print(numpy.get_include())"
        RESULT_VARIABLE RETCODE_FIND_NUMPY_INCLUDE
        OUTPUT_VARIABLE RESULT_FIND_NUMPY_INCLUDE
        ERROR_VARIABLE RESULT_FIND_NUMPY_INCLUDE
    )

    if(${RETCODE_FIND_NUMPY_INCLUDE} GREATER 0)
        message(FATAL_ERROR "Could not obtain numpy include directory")
    endif()

    # Save the result
    set(NUMPY_FOUND TRUE CACHE BOOL "The Numpy library")
    set(NUMPY_INCLUDE_DIRS ${RESULT_FIND_NUMPY_INCLUDE}
        CACHE PATH "Path to Numpy includes")
    mark_as_advanced(NUMPY_FOUND NUMPY_INCLUDE_DIRS)

    message(STATUS "Found Numpy include dirs ${NUMPY_INCLUDE_DIRS}")

endif()
endif()
